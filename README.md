## How to play the game ##
1. Download a zip file [ZergDefense_1.0.zip](https://bitbucket.org/smejkapa/zerg-defense/downloads/ZergDefense_1.0.zip). (Also in downloads section).
1. Unzip and run `zergDefense.exe`
1. Enter to start the game
1. Click on tiles to select them and then press number according to action you want to perform (list on the right).

To edit the map or config simply open `level0.cfg/map` in notepad and change whatever you wish.


### What is this repository for? ###

Simple C++ tower defense game.

### How do I get set up? ###

* Clone repository
* Download SFML2.1.zip from Downloads section
* Unpack SFML2.1.zip to C:\ (if you choose another directory, you will need to modify project appropriately)
* SFML is statically linked, no dlls in project folder needed.
* Build