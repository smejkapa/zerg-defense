#include "Action.h"
#include <iostream>

using namespace std;
// Build action
bool BuildAction::IsAvailable(Tile* tile, Player& player)
{
	if(tile->IsBuildable() && player.CanBuy(building->GetPrice()))
		return true;

	return false;
}
bool BuildAction::CheckAndExecute(Tile* tile, Player& player)
{
	if(IsAvailable(tile, player))
	{
		cout << "Executing build action: " << building->GetName() << endl;
		player.Buy(building->GetPrice());
		shared_ptr<Building> b = building->Build(player.GetContent(), tile);
		player.AddBuilding(b);
		return true;
	}
	else
	{
		cout << "Build action not avaliable" << endl; 
		return false;
	}
}
string BuildAction::GetName()
{
	return name + " " + building->GetName() + " [-$" + to_string(building->GetPrice()) + "]";
}

// Upgrade action
bool UpgradeAction::IsAvailable(Tile* tile, Player& player)
{
	if(tile->GetBuilding() != nullptr && player.CanBuy(tile->GetBuilding()->GetUpgradePrice()))
	{
		return true;
	}
	return false;
}
bool UpgradeAction::CheckAndExecute(Tile* tile, Player& player)
{
	if(IsAvailable(tile, player))
	{
		cout << "Executing upgrade action" << endl;
		player.Buy(tile->GetBuilding()->GetUpgradePrice());
		tile->GetBuilding()->Upgrade();
		return true;
	}
	else
	{
		cout << "Upgrade action not avaliable" << endl;
		return false;
	}
}
string UpgradeAction::GetName()
{
	return building == nullptr ? name : name + " [-$" + to_string(building->GetUpgradePrice()) + "]";
}

// Destroy action
bool DestroyAction::IsAvailable(Tile* tile, Player& player)
{
	if(tile->GetBuilding() != nullptr)
		return true;
	return false;
}
bool DestroyAction::CheckAndExecute(Tile* tile, Player& player)
{
	if(IsAvailable(tile, player))
	{
		cout << "Executing destroy action" << endl;
		player.Earn(tile->GetBuilding()->GetSalvageMoney());
		player.RemoveBuilding(tile->GetBuilding());
		tile->RemoveBuilding();
		return true;
	}
	else
	{
		cout << "Destroy action not avaliable" << endl;
		return false;
	}
}
string DestroyAction::GetName()
{
	return building == nullptr ? name : name + " [+$" + to_string(building->GetSalvageMoney()) + "]";
}