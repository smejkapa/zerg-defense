#ifndef ACTION_H
#define ACTION_H
#include <string>
#include <memory>
#include <SFML\Audio.hpp>
#include "Tile.h"
#include "Player.h"
#include "Building.h"

class Player;
class Tile;
class Building;
class Action
{
public:
	Action(std::string name) : name(name) {}
	virtual std::string GetName() {return name;}
	virtual bool IsAvailable(Tile* tile, Player& player)=0;
	virtual bool CheckAndExecute(Tile* tile, Player& player)=0;
protected:
	std::string name;
	int price;
};
class BuildAction : public Action
{
public:
	BuildAction(std::shared_ptr<Building>& building) : Action("Build"), building(building) {}
	virtual bool IsAvailable(Tile* tile, Player& player) override;
	virtual bool CheckAndExecute(Tile* tile, Player& player) override;
	virtual std::string GetName() override;
private:
	std::shared_ptr<Building> building;
};
class UpgradeAction : public Action
{
public:
	UpgradeAction(std::shared_ptr<Building>& building) : Action("Upgrade"), building(building) {}
	virtual bool IsAvailable(Tile* tile, Player& player) override;
	virtual bool CheckAndExecute(Tile* tile, Player& player) override;
	virtual std::string GetName() override;
private:
	std::shared_ptr<Building> building; // Building reference - what do we potentially want to upgrade - nullptr if nothing
};
class DestroyAction : public Action
{
public:
	DestroyAction(std::shared_ptr<Building>& building) : Action("Salvage"), building(building) {}
	virtual bool IsAvailable(Tile* tile, Player& player) override;
	virtual bool CheckAndExecute(Tile* tile, Player& player) override;
	virtual std::string GetName() override;
private:
	std::shared_ptr<Building> building;
};
#endif