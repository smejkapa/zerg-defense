#ifndef ANIMATION_H
#define ANIMATION_H
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

class Animation
{
public:
	Animation()
		: texture(nullptr), frameWidth(0), isLooping(false), frameTime(0) {}
	Animation(sf::Texture* texture, int frameWidth, bool isLooping, float frameTime)
		: texture(texture), frameWidth(frameWidth), isLooping(isLooping), frameTime(frameTime) {}
	float GetFrameTime() {return frameTime;}
	int GetFrameWidth() {return frameWidth;}
	int GetFrameHeight() {return texture->getSize().y;}
	bool IsLooping() {return isLooping;}
	int GetFrameCount() {return texture->getSize().x / frameWidth;}
	void SetSpeed(float speed) {frameTime = speed;}
	sf::Texture* GetTexture() {return texture;}
private:
	sf::Texture* texture;
	int frameWidth;
	bool isLooping;
	float frameTime;
};

#endif