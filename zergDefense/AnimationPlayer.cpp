#include "AnimationPlayer.h"
#include "CustomExceptions.h"

using namespace std;
using namespace sf;

AnimationPlayer::AnimationPlayer(Animation& animation)
	: animation(&animation)
{
	time = 0.0f;
	frameIndex = 0;
}
void AnimationPlayer::PlayAnimation(Animation& newAnim)
{
	// Don't restart animation if already playing
	if(animation == &newAnim)
		return;

	// Set new animation
	animation = &newAnim;
	frameIndex = 0;
	time = 0.0f;
	sprite.setTexture(*animation->GetTexture());
	sprite.setOrigin(animation->GetFrameWidth() / 2.f, animation->GetFrameHeight() / 2.f);
}
void AnimationPlayer::PrepareNextFrame(Time& elapsed, AnimationPlayer::Direction direction)
{
	if(animation == nullptr)
		throw Exception("No animation found");

	time += elapsed.asSeconds();
	while(time > animation->GetFrameTime())
	{
		time -= animation->GetFrameTime();
		// Move the window one frame forward
		if(animation->IsLooping())
			frameIndex = ++frameIndex % animation->GetFrameCount();
		else
			frameIndex = frameIndex + 1 < animation->GetFrameCount() ? frameIndex + 1 : animation->GetFrameCount() - 1;
	}

	sprite.setTextureRect(IntRect(frameIndex * animation->GetFrameWidth(), 0, animation->GetFrameWidth(), animation->GetFrameHeight()));
	Turn(direction);
}
void AnimationPlayer::Draw(RenderWindow& window, Vector2f& position)
{
	sprite.setPosition(position);
	window.draw(sprite);
}
void AnimationPlayer::Turn(AnimationPlayer::Direction direction)
{
	// Default is right
	switch (direction)
	{
	case AnimationPlayer::Right:
		sprite.setRotation(0);
		break;
	case AnimationPlayer::Left:
		sprite.setRotation(180);
		break;
	case AnimationPlayer::Top:
		sprite.setRotation(270);
		break;
	case AnimationPlayer::Bot:
		sprite.setRotation(90);
		break;
	default:
		break;
	}
}