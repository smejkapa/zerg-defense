#ifndef ANIMATION_PLAYER_H
#define ANIMATION_PLAYER_H
#include "Animation.h"

class AnimationPlayer
{
public:
	enum Direction {Right, Left, Top, Bot};
	
	AnimationPlayer() : animation(nullptr) {}
	AnimationPlayer(Animation&);
	// Removes old and sets new animation to play
	void PlayAnimation(Animation&);
	// Call this method in Update each frame
	void PrepareNextFrame(sf::Time& elapsed, AnimationPlayer::Direction);
	// Call this method in Draw each frame
	void Draw(sf::RenderWindow& window, sf::Vector2f& center);
private:
	void Turn(AnimationPlayer::Direction);
	sf::Sprite sprite;
	Animation* animation;
	float time;
	int frameIndex;
};

#endif