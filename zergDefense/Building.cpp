#include <iomanip>
#include "Building.h"
#include "CustomExceptions.h"
#include "ContentHolder.h"
#include <math.h>

using namespace sf;
using namespace std;

// Building
Building::Building(ContentHolder& content, int price, string texturePath, Player* owner) : price(price), owner(owner)
{
	// Tile can be nullptr if building is only "virtual" it is not placed anywhere
	LoadContent(content, texturePath);
	sprite = Sprite(*texture);
	place = nullptr;
}
void Building::PlaceBuilding(Tile* tile, shared_ptr<Building>& building)
{
	place = tile;
	if(tile != nullptr)
	{
		tile->SetBuilding(building);
		if(texture->getSize().x > tile->getSize().x || texture->getSize().y > tile->getSize().y)
			throw Exception("Building texture si bigger than tile");
		position = Vector2f(tile->GetPosition().x, tile->GetPosition().y);
	}
	else
		position = Vector2f(0,0);

	sprite.setPosition(position.x, position.y);
}
void Building::LoadContent(ContentHolder& content, string texturePath)
{
	if((texture = content.GetTexture(texturePath)) == nullptr)
		texture = content.LoadTexture(texturePath);
}
void Building::Draw(RenderWindow& window)
{
	window.draw(sprite);
}

// Tower
const int Tower::expTable[] = {0, 200, 500, 900, 1400, 2000, 2600, 3200, 4400, 5400, 6000, 8200, 9000, 10400, 11900, 13500, 15200, 17000, 18900, 20900, 23000, 25200, 27500, 29900, 32400, 0};
const int Tower::maxLevel = 25;
Tower::Tower(ContentHolder& content, Level* level, int price,  string texturePath, string shotSoundPath, Player* owner) : Building(content, price, texturePath, owner), level(level) 
{
	LoadContent(content, shotSoundPath);
	experience = 0;
	nextLvlExp = 200;
	expLevel = 1;
	upgradeLevel = 0;
	damage = 0;
	attackCooldown = 0;
	attackRange = 0;
	killMoneyMultiplier = 1.0f;
	lastShotTime = 1000;
	target = nullptr;
	shotSnd.setVolume(2);
}
void Tower::LoadContent(ContentHolder& content, string& shotSndPath)
{
	SoundBuffer* sb;
	if((sb = content.GetSoundBuffer(shotSndPath)) == nullptr)
		sb = content.LoadSoundBuffer(shotSndPath);
	shotSnd.setBuffer(*sb);
}
// Draw
void Tower::Draw(RenderWindow& window)
{
	Building::Draw(window);
	
	if(place->IsSelected()) 
	{
		// Simple way to draw the range, but not so nice, newer towers may hide this range indicator
		// Draw range circle
		CircleShape range;
		range.setPointCount(100);
		range.setRadius(attackRange);
		range.setFillColor(Color(0, 255, 0, 30));
		range.setOutlineColor(Color::Green);
		range.setOutlineThickness(1);
		range.setPosition(position.x + 32 - attackRange, position.y + 32 - attackRange);
		window.draw(range);
	}

	if(target != nullptr)
	{
		Vertex line[] = 
		{
			Vertex(Vector2f(position.x + texture->getSize().x / 2, position.y + texture->getSize().y / 2)),
			Vertex(Vector2f(target->GetCenter().x, target->GetCenter().y))
		};
		window.draw(line, 2, Lines);
	}

	// Draw the level and experience
	// TODO
}
void Tower::DrawDetails(RenderWindow& window, Text& text, Vector2i& offset)
{
	Level::DrawString(window, text, offset, Text::Style::Underlined, GetName());
	Level::DrawString(window, text, offset, Text::Style::Regular, "Level: " + to_string(expLevel));
	Level::DrawString(window, text, offset, Text::Style::Regular, "Experience: " + to_string(experience) + "/" + to_string(nextLvlExp));
	Level::DrawString(window, text, offset, Text::Style::Regular, "Kill money multiplier: " + to_string(killMoneyMultiplier).substr(0,3));
	Level::DrawString(window, text, offset, Text::Style::Regular, "Upgrades: " + to_string(upgradeLevel));
	Level::DrawString(window, text, offset, Text::Style::Regular, "Damage: " + to_string(damage));
	Level::DrawString(window, text, offset, Text::Style::Regular, "Range: " + to_string(attackRange));
	std::setprecision(3);
	Level::DrawString(window, text, offset, Text::Style::Regular, "Cooldown: " + to_string(attackCooldown).substr(0,4) + " sec");
}
// Update
void Tower::Update(Time& elapsed, RenderWindow& window)
{
	if(experience >= nextLvlExp && expLevel < maxLevel)
		LevelUp();

	lastShotTime += elapsed.asSeconds();
	// Enemy might die from another tower
	// This means there is no overkill when aiming at enemies
	// There is an overkill when killing because of projectile travel speed
	if(target == nullptr || target->IsDead() || target->IsFinished()) 
		FindTarget();
	else if( GetDistance(target) > attackRange)
	{
		FindTarget();
	}

	// Maybe we have not found any target
	if(target != nullptr)
	{
		if(lastShotTime > attackCooldown)
		{
			Shoot();
			lastShotTime = 0;
		}
	}
}
void Tower::FindTarget()
{
	// Find the closest enemy and set it as target
	// Don't need to check for dead enemies - they are removed as soon as they die
	// Don't need to check for finished enemies - the are removed before player update happens
	// In other words -> Level->GetEnemies() returns always fresh list
	target = nullptr;
	const Level::EnemyVector enemies = level->GetEnemies();
	if(enemies.size() > 0)
	{
		Vector2f center = Vector2f(position.x + texture->getSize().x / 2, position.y + texture->getSize().y / 2);
		int closest = 0;
		float bestDistanceSqr = pow(enemies[0]->GetCenter().x - center.x, 2) + pow(enemies[0]->GetCenter().y - center.y, 2);
		for(size_t i = 1; i < enemies.size(); ++i)
		{
			float distanceSqr = pow(enemies[i]->GetCenter().x - center.x, 2) + pow(enemies[i]->GetCenter().y - center.y, 2);
			if(distanceSqr < bestDistanceSqr)
			{
				bestDistanceSqr = distanceSqr;
				closest = i;
			}
		}

		// check if closest enemy is in our range
		if(sqrt(bestDistanceSqr) <= attackRange)
			target = enemies[closest];
	}
}
void Tower::Shoot()
{
	target->TakeDamage(damage);
	if(target->IsDead())
	{
		// This check might not be necessary since this we don't shoot at dead enemies
		if(level->Kill(target)) 
		{
			experience += target->GetXpBounty();
			owner->Earn(target->GetMoneyBounty());
		}
		target = nullptr;
	}

	// If we play sounds as tower shoots it might get very messy for fast shooting towers
	if(shotSnd.getStatus() != shotSnd.Playing)
		shotSnd.play();
}
float Tower::GetDistance(shared_ptr<Enemy>& e)
{
	return sqrt(pow(e->GetCenter().x - (position.x + texture->getSize().x / 2), 2) + pow(e->GetCenter().y - (position.y + texture->getSize().y / 2), 2));
}
void Tower::LevelUp()
{
	nextLvlExp = Tower::expTable[expLevel + 1];
	++expLevel;
	killMoneyMultiplier += .2f;
}
void Tower::Upgrade()
{
	++upgradeLevel;
}

// BasicTower
const char *tinitB[] = {"Textures/Towers/basicTower.png"};
std::vector<std::string> BasicTower::textures(tinitB, end(tinitB));
const char *sinitB[] = {"Sounds/basicTower.wav"};
std::vector<std::string> BasicTower::sounds(sinitB, end(sinitB));
void BasicTower::PreLoadResources(ContentHolder& content)
{
	for(auto i = sounds.begin(); i != sounds.end(); ++i)
		content.LoadSoundBuffer(*i);
	for(auto i = textures.begin(); i != textures.end() ; ++i)
		content.LoadTexture(*i);
}
BasicTower::BasicTower(ContentHolder& content, Level* level, Player* owner) : Tower(content, level, 100, "Textures/Towers/basicTower.png", "Sounds/basicTower.wav", owner) 
{
	damage = 10;
	attackCooldown = 0.5f;
	attackRange = texture->getSize().x * 1.5f; // 1.5 because we mesure from the middle of our tile
}
shared_ptr<Building> BasicTower::Build(ContentHolder& content, Tile* position)
{
	shared_ptr<Building> sp = shared_ptr<Building>(new BasicTower(content, level, owner));
	sp->PlaceBuilding(position, sp);
	return sp;
}
void BasicTower::Update(Time& elapsed, RenderWindow& window)
{
	Tower::Update(elapsed, window);
}
void BasicTower::Shoot()
{
	shared_ptr<Projectile> p = shared_ptr<Projectile>(new Projectile(owner, this, target, 150, GetCenter(), damage));
	owner->AddProjectile(p);
}
void BasicTower::Upgrade()
{
	Tower::Upgrade();
	damage += 5;
}

// LongRangeTower
const char *tinitL[] = {"Textures/Towers/longRangeTower.png"};
std::vector<std::string> LongRangeTower::textures(tinitL, end(tinitL));
const char *sinitL[] = {"Sounds/longRangeTower.wav"};
std::vector<std::string> LongRangeTower::sounds(sinitL, end(sinitL));
void LongRangeTower::PreLoadResources(ContentHolder& content)
{
	for(auto i = sounds.begin(); i != sounds.end(); ++i)
		content.LoadSoundBuffer(*i);
	for(auto i = textures.begin(); i != textures.end() ; ++i)
		content.LoadTexture(*i);
}
LongRangeTower::LongRangeTower(ContentHolder& content, Level* level, Player* owner) : Tower(content, level, 150, "Textures/Towers/longRangeTower.png", "Sounds/longRangeTower.wav", owner) 
{
	damage = 1;
	attackCooldown = .1f; // Constant laser beam
	attackRange = texture->getSize().x * 3;
}
shared_ptr<Building> LongRangeTower::Build(ContentHolder& content, Tile* position)
{
	shared_ptr<Building> sp = shared_ptr<Building>(new LongRangeTower(content, level, owner));
	sp->PlaceBuilding(position, sp);
	return sp;
}
void LongRangeTower::Update(Time& elapsed, RenderWindow& window)
{
	Tower::Update(elapsed, window);
}
void LongRangeTower::Shoot()
{
	shared_ptr<Projectile> p = shared_ptr<Projectile>(new Projectile(owner, this, target, 200, GetCenter(), damage));
	owner->AddProjectile(p);
}
void LongRangeTower::Upgrade()
{
	Tower::Upgrade();
	damage += 1;
}

// Projectile
float GetDistance2(Vector2f& u, Vector2f& v)
{
	return sqrt(pow(u.x - v.x, 2) + pow(u.y - v.y, 2));
}
Projectile::Projectile(Player* owner, Tower* producer, shared_ptr<Enemy>& target, float speed, Vector2f pos, int damage)
	: owner(owner), producer(producer), target(target), speed(speed), damage(damage)
{
	done = false;
	position = pos;
	epsilon = 16;
	shape = CircleShape(4);
	shape.setFillColor(Color::Black);
	shape.setOrigin(4, 4); // in the middle
	targetPosition = target->GetCenter();
}
void Projectile::Update(Time& elapsed, RenderWindow& window)
{
	// Remove dead or finished (invulnerable) targets
	if(target != nullptr && (target->IsDead() || target->IsFinished()))
		target = nullptr;

	if(target != nullptr)
		targetPosition = target->GetCenter();

	// Move
	float distance = speed * elapsed.asSeconds();
	float toTarget = GetDistance(targetPosition);
	if(distance >= toTarget - epsilon)
	{
		// Hit and let's be destroyed
		done = true;
		if(target != nullptr)
		{
			target->TakeDamage(damage);
			if(target->IsDead())
			{
				owner->ProcessKill(this);
			}
		}
	}
	else
	{
		// Moves closer but does not hit yet
		// Get vector from position to target -> normalize it = direction vector -> multipy by distace = real vector with correct distace to travel
		Vector2f travel = Vector2f(((targetPosition.x - position.x) / toTarget) * distance, ((targetPosition.y - position.y) / toTarget) * distance);
		position.x += travel.x;
		position.y += travel.y;
		shape.setPosition(position.x, position.y);
	}
}
float Projectile::GetDistance(Vector2f& p)
{
	return sqrt(pow(p.x - position.x, 2) + pow(p.y - position.y, 2));
}
void Projectile::Draw(RenderWindow& window)
{
	window.draw(shape);
}