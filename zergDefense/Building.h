#ifndef BUILDING_H
#define BUILDING_H
#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>
#include <string>
#include <iostream>
#include <memory>
#include "IUpdatable.h"
#include "IDrawable.h"
#include "Tile.h"
#include "ContentHolder.h"
#include "Level.h"
#include "Enemy.h"
#include "Player.h"

/**************
* Building may be modified so it is not build instantly, but over time
* Add build time and one condition to Update() and maybe some animation/progress bar
***************/
class Tile;
class Level;
class Player;
class Building : public IUpdatable, public IDrawable
{
public:
	Building(ContentHolder& content, int price, std::string texturePath, Player* owner);
	virtual ~Building() {place = nullptr;}
	
	Tile* GetPlace() {return place;}
	int GetPrice() {return price;}
	virtual int GetUpgradePrice()=0;
	virtual int GetSalvageMoney()=0;
	// The actual act of upgrading
	virtual void Upgrade()=0;
	
	virtual std::shared_ptr<Building> Build(ContentHolder& content, Tile*)=0;
	void PlaceBuilding(Tile* tile, std::shared_ptr<Building>&);
	
	virtual std::string GetName()=0;
	virtual void Draw(sf::RenderWindow&) override;
	virtual void DrawDetails(sf::RenderWindow&, sf::Text&, sf::Vector2i&)=0;
protected:
	Tile* place; // Position of the building
	Player* owner;
	int price;

	void LoadContent(ContentHolder& content, std::string texturePath);
};

class Tower : public Building
{
public:
	Tower(ContentHolder& content, Level* level, int price, std::string texturePath, std::string, Player*);
	virtual ~Tower() {};
	virtual void Draw(sf::RenderWindow&) override;
	virtual void DrawDetails(sf::RenderWindow&, sf::Text&, sf::Vector2i&) override;
	virtual void Update(sf::Time&, sf::RenderWindow&) override;
	void GainExp(int xp) {experience += xp;}
	float GetMoneyMultiplier() {return killMoneyMultiplier;}
	sf::Vector2f GetCenter() {return sf::Vector2f(position.x + texture->getSize().x / 2, position.y + texture->getSize().y /2);}
	virtual void Upgrade() override;
protected:
	// Game logic
	int experience;
	int nextLvlExp;
	int expLevel;
	int upgradeLevel;
	int damage;
	float killMoneyMultiplier;
	float attackCooldown; // time between 2 attacks
	int attackRange;      // range in pixels
	float lastShotTime;
	void LevelUp();
	static const int maxLevel;
	static const int expTable[];
	
	Level* level;

	// Resources
	
	sf::Sound shotSnd;
	void LoadContent(ContentHolder& content, std::string& shotSndPath);
	
	// Targeting enemies
	std::shared_ptr<Enemy> target;
	void FindTarget();
	virtual void Shoot();
	inline float GetDistance(std::shared_ptr<Enemy>&);
};

class BasicTower : public Tower
{
public:
	BasicTower(ContentHolder& content, Level*, Player*);
	virtual ~BasicTower() {};
	virtual std::shared_ptr<Building> Build(ContentHolder& content, Tile*) override;
	virtual void Update(sf::Time&, sf::RenderWindow&);
	virtual std::string GetName() override {return "Basic Tower";}
	virtual int GetUpgradePrice() override {return upgradeLevel * 100 + 100;}
	virtual int GetSalvageMoney() override {return (upgradeLevel * 100 + 100) / 2;}
	virtual void Upgrade() override;
	static void PreLoadResources(ContentHolder&);
protected:
	virtual void Shoot() override;
private:
	static std::vector<std::string> sounds;
	static std::vector<std::string> textures;
};

class LongRangeTower : public Tower
{
public:
	LongRangeTower(ContentHolder& content, Level*, Player*);
	virtual ~LongRangeTower() {};
	virtual std::shared_ptr<Building> Build(ContentHolder& content, Tile*) override;
	virtual void Update(sf::Time&, sf::RenderWindow&);
	virtual std::string GetName() override {return "Long Range Tower";}
	virtual int GetUpgradePrice() override {return upgradeLevel * 100 + 200;}
	virtual int GetSalvageMoney() override {return (upgradeLevel * 100 + 100) / 2;}
	virtual void Upgrade() override;
	static void PreLoadResources(ContentHolder&);
protected:
	virtual void Shoot() override;
private:
	static std::vector<std::string> sounds;
	static std::vector<std::string> textures;
};

class Projectile : public IUpdatable, public IDrawable
{
public:
	Projectile(Player* owner, Tower* producer, std::shared_ptr<Enemy>& target, float speed, sf::Vector2f position, int damage);
	virtual void Update(sf::Time& elapsed, sf::RenderWindow& window) override;
	virtual void Draw(sf::RenderWindow& window) override;
	std::shared_ptr<Enemy>& GetTarget() {return target;} 
	Tower* GetTower() {return producer;}
	bool IsDone() {return done;}
private:
	sf::CircleShape shape;
	std::shared_ptr<Enemy> target;
	sf::Vector2f targetPosition;
	float speed;
	int damage;
	float epsilon; // Accuracy - the projectile will hit even if it is epsilon far from target
	bool done;
	Tower* producer;
	Player* owner;

	inline float GetDistance(sf::Vector2f&);
};
#endif