#ifndef CHECKPOINT_H
#define CHECKPOINT_H
#include <SFML\Window.hpp>
#include <SFML\System.hpp>

class Checkpoint
{
public:
	Checkpoint(sf::Vector2f position, sf::Vector2f gridCoordinates, bool isEnd) : position(position), gridCoordinates(gridCoordinates), end(isEnd) {}
	bool IsEnd() {return end;}
	sf::Vector2f GetPosition() {return position;}
private:
	sf::Vector2f position; // Absolute precise position
	sf::Vector2f gridCoordinates; // Position in the map
	bool end;
};

#endif