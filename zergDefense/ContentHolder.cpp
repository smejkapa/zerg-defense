#include "ContentHolder.h"
#include "CustomExceptions.h"
#include <algorithm>
#include <iostream>

using namespace sf;
using namespace std;

Texture* ContentHolder::GetTexture(string& texturePath)
{
	auto texture = textures.find(texturePath);
	if(texture != textures.end())
		return &texture->second;
	return nullptr;
}
Texture* ContentHolder::LoadTexture(string& texturePath)
{	
	cout << "Loading texture: " << texturePath << endl;
	Texture texture;
	if(!texture.loadFromFile(texturePath))
		throw Exception("Can not load texture file: " + texturePath);
	
	textures.insert(make_pair(texturePath, texture));

	return &textures[texturePath];
}

SoundBuffer* ContentHolder::GetSoundBuffer(string& path)
{
	auto sb = soundBuffers.find(path);
	if(sb != soundBuffers.end())
		return &sb->second;
	return nullptr;
}
SoundBuffer* ContentHolder::LoadSoundBuffer(string& path)
{
	cout << "Loading sound: " << path << endl;
	SoundBuffer sb;
	if(!sb.loadFromFile(path))
		throw Exception("Can not load sound file: " + path);

	soundBuffers.insert(make_pair(path, sb));

	return &soundBuffers[path];
}

Font* ContentHolder::GetFont(string& path)
{
	auto f = fonts.find(path);
	if(f != fonts.end())
		return &f->second;
	return nullptr;
}
Font* ContentHolder::LoadFont(string& path)
{
	cout << "Loading font: " << path << endl;
	Font f;
	if(!f.loadFromFile(path))
		throw Exception("Can not load font: " + path);

	fonts.insert(make_pair(path, f));

	return &fonts[path];
}