#ifndef CONTENT_HOLDER_H
#define CONTENT_HOLDER_H
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <map>
#include <string>

///
/// Class holding resources
///
class ContentHolder
{
public:
	sf::Texture* GetTexture(std::string& path);
	sf::Texture* LoadTexture(std::string& path);
	sf::SoundBuffer* GetSoundBuffer(std::string& path);
	sf::SoundBuffer* LoadSoundBuffer(std::string& path);
	sf::Font* GetFont(std::string& path);
	sf::Font* LoadFont(std::string& path);
private:
	typedef std::map<std::string, sf::Texture> Textures; 
	Textures textures;

	typedef std::map<std::string, sf::SoundBuffer> SoundBuffers;
	SoundBuffers soundBuffers;

	typedef std::map<std::string, sf::Font> Fonts;
	Fonts fonts;
};

#endif