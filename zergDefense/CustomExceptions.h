#ifndef CUSTOMEXCEPTIONS_H
#define CUSTOMEXCEPTIONS_H
#include <stdexcept>
#include <string>

class Exception : public std::exception
{
public:
	explicit Exception(std::string s) : message(s)
	{ }
	std::string GetMessage() {return message;}
private:
	std::string message;
};

#endif