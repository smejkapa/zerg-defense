#include "Enemy.h"
#include "CustomExceptions.h"
#include "ContentHolder.h"

using namespace std;
using namespace sf;

///////////
// Enemy //
///////////
Enemy::Enemy(sf::Vector2f& center, ContentHolder& content, int hp, float speed, Enemy::Path* path, std::string& texturePath) 
	: currentHP(hp), maxHP(hp), speed(speed), path(path) , center(center), finished(false)
{
	LoadContent(content, texturePath);
	position = Vector2f(center.x - run.GetFrameWidth() / 2, center.y - run.GetFrameHeight() / 2);
	
	goalIndex = 1;
	currentGoal = &((*path)[goalIndex]);
	
	// HP bar
	hpBarSize = Vector2f(32, 6);
	hpBar = RectangleShape(hpBarSize);
	hpBarOutline = RectangleShape(hpBarSize);
	hpBarOutline.setFillColor(Color(255, 255, 255, 0));
	hpBarOutline.setOutlineColor(Color::Black);
	hpBarOutline.setOutlineThickness(1);
}
void Enemy::LoadContent(ContentHolder& content, string& texturePath)
{
	if((texture = content.GetTexture(texturePath)) == nullptr)
		texture = content.LoadTexture(texturePath);
	run = Animation(texture, texture->getSize().y, true, .3f);
	spriteAnim.PlayAnimation(run);
}
// Draw
void Enemy::Draw(RenderWindow& window)
{
	spriteAnim.Draw(window, center);
	
	// Draw HP
	float percent = (float)currentHP / maxHP;
	int green = 255;
	int red = 255 * (1 - percent) * 2;
	if(red >= 255)
	{
		red = 255;
		green = 255 * percent;
	}
	Color color(red, green, 0);
	hpBar.setFillColor(color);
	hpBar.setPosition(center.x - 16, center.y - 20);
	hpBar.setSize(Vector2f(hpBarSize.x * percent, hpBarSize.y));
	hpBarOutline.setPosition(center.x - 16, center.y - 20);
	window.draw(hpBar);
	window.draw(hpBarOutline);
}
// Update
void Enemy::Update(Time& elapsed, RenderWindow& window)
{
	AnimationPlayer::Direction dir = Move(elapsed);
	UpdatePosition();
	spriteAnim.PrepareNextFrame(elapsed, dir);
}
AnimationPlayer::Direction Enemy::Move(Time& elapsed)
{
	AnimationPlayer::Direction dir = AnimationPlayer::Direction::Right;
	float distance = elapsed.asSeconds() * speed;

	if(center.x == currentGoal->GetPosition().x)
	{
		// Move along Y
		if(center.y < currentGoal->GetPosition().y)
		{
			dir = AnimationPlayer::Direction::Bot;
			center.y += distance;
			if(center.y > currentGoal->GetPosition().y) // We crossed the goal
				center.y = currentGoal->GetPosition().y;
		}
		else
		{
			dir = AnimationPlayer::Direction::Top;
			center.y -= distance;
			if(center.y < currentGoal->GetPosition().y)
				center.y = currentGoal->GetPosition().y;
		}
	}
	else if( center.y == currentGoal->GetPosition().y)
	{
		// Move along X
		if(center.x < currentGoal->GetPosition().x)
		{
			dir = AnimationPlayer::Direction::Right;
			center.x += distance;
			if(center.x > currentGoal->GetPosition().x)
				center.x = currentGoal->GetPosition().x;
		}
		else
		{
			dir = AnimationPlayer::Direction::Left;
			center.x -= distance;
			if(center.x < currentGoal->GetPosition().x)
				center.x = currentGoal->GetPosition().x;
		}
	}
	else
		throw Exception("Movement exception");

	if(center == currentGoal->GetPosition())
		GetNextGoal();

	return dir;
}
void Enemy::GetNextGoal()
{
	if(currentGoal->IsEnd())
		Finish();
	else
	{
		++goalIndex;
		currentGoal = &((*path)[goalIndex]);
	}
}
void Enemy::UpdatePosition() 
{
	position = sf::Vector2f(center.x - run.GetFrameWidth() / 2, center.y - run.GetFrameHeight() / 2);
}
//////////
// Ling //
//////////
string Ling::texturePath = "Textures/Enemies/lingRun.png";
Ling::Ling(Vector2f& center, ContentHolder& content, Enemy::Path* path, int buffLevel)
	: Enemy(center, content, 80 + buffLevel * 10, 60 + buffLevel * 5, path, texturePath)
{
	run.SetSpeed(.2f);
	xpBounty = 10;
	moneyBounty = 5;
}
void Ling::Update(Time& elapsed, RenderWindow& window)
{
	Enemy::Update(elapsed, window);
}

///////////
// Roach //
///////////
string Roach::texturePath = "Textures/Enemies/roachRun.png";
Roach::Roach(Vector2f& center, ContentHolder& content, Enemy::Path* path, int buffLevel)
	: Enemy(center, content, 300 + buffLevel * 50, 30 + buffLevel * 3, path, texturePath)
{
	xpBounty = 30;
	moneyBounty = 20; 
}
void Roach::Update(Time& elapsed, RenderWindow& window)
{
	Enemy::Update(elapsed, window);
}