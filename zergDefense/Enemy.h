#ifndef ENEMY_H
#define ENEMY_H
#include <string>
#include "IUpdatable.h"
#include "IDrawable.h"
#include "Checkpoint.h"
#include "ContentHolder.h"
#include "AnimationPlayer.h"
#include <iostream>

class Enemy : public IUpdatable, public IDrawable
{
public:
	typedef std::vector<Checkpoint> Path;

	Enemy(sf::Vector2f& center, ContentHolder& content, int hp, float speed, Enemy::Path* path, std::string&);
	virtual ~Enemy() {}
	virtual void Draw(sf::RenderWindow&) override;
	virtual void Update(sf::Time&, sf::RenderWindow&) override;
	bool IsDead() {return currentHP <= 0;}
	bool IsFinished() {return finished;}
	
	sf::Vector2f GetCenter() {return center;}
	void TakeDamage(int damage) {currentHP -= damage;}
	int GetXpBounty() {return xpBounty;}
	int GetMoneyBounty() {return moneyBounty;}
protected:
	void Finish() {finished = true;}
	int maxHP;
	int currentHP;
	float speed;
	int xpBounty;
	int moneyBounty;
	int goalIndex;
	Checkpoint* currentGoal;
	Enemy::Path* path;
	sf::Vector2f center;
	sf::Vector2f hpBarSize;
	sf::RectangleShape hpBar;
	sf::RectangleShape hpBarOutline;
	
	// Graphics
	Animation run;
	AnimationPlayer spriteAnim;

	inline void UpdatePosition();
	AnimationPlayer::Direction Move(sf::Time&);
	bool finished;
	void GetNextGoal();
private:
	void LoadContent(ContentHolder& content, std::string& texturePath);
};

class Ling : public Enemy
{
public:
	Ling(sf::Vector2f& position, ContentHolder& content, Enemy::Path* path, int buffLevel);
	virtual ~Ling() {}
	virtual void Update(sf::Time&, sf::RenderWindow&) override;
private:
	static std::string texturePath;
};

class Roach : public Enemy
{
public:
	Roach(sf::Vector2f& position, ContentHolder& content, Enemy::Path* path, int buffLevel);
	virtual ~Roach() {}
	virtual void Update(sf::Time&, sf::RenderWindow&) override;
private:
	static std::string texturePath;
};

#endif