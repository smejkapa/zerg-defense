#ifndef ENTITY_H
#define ENTITY_H
#include <SFML/System.hpp>
#include "IUpdatable.h"
#include "IDrawable.h"

class Entity : public IUpdatable, public IDrawable
{
public:
	sf::Vector2f Position() {return position;}
private:
	sf::Vector2f position;
};

#endif