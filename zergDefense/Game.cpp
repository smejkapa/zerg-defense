#include "Game.h"
#include "Building.h"
#include "CustomExceptions.h"

using namespace std;
using namespace sf;

Game::Game()
{
	level = unique_ptr<Level>(new Level(this));
	windowSize = Vector2i(1024, 640);
	started = false;
	if (!arial.loadFromFile("Fonts/arial.ttf"))
		throw Exception("Font not found: Fonts/arial.ttf");

	startGame = Text("Press ENTER to start the game", arial, 40);
	startGame.setColor(Color::Black);
	startGame.setPosition(windowSize.x * 0.25f, windowSize.y * 0.45f);

	startRect = RectangleShape(Vector2f(startGame.getGlobalBounds().width + 32, startGame.getGlobalBounds().height + 32));
	startRect.setPosition(startGame.getGlobalBounds().left - 16, startGame.getGlobalBounds().top - 16);
	startRect.setFillColor(Color(255, 255, 255, 200));

	pausedText = Text("Press SPACE to unpause the game", arial, 40);
	pausedText.setColor(Color::Black);
	pausedText.setPosition(windowSize.x * 0.25f, windowSize.y * 0.45f);

	pausedRect = RectangleShape(Vector2f(pausedText.getGlobalBounds().width + 32, pausedText.getGlobalBounds().height + 32));
	pausedRect.setPosition(pausedText.getGlobalBounds().left - 16, pausedText.getGlobalBounds().top - 16);
	pausedRect.setFillColor(Color(255, 255, 255, 200));

	paused = false;
	gameOver = false;
}
void Game::Run()
{
	ContextSettings settings;
	settings.antialiasingLevel = 8;
	RenderWindow window(VideoMode(windowSize.x, windowSize.y), "Zerg Defense", Style::Titlebar | Style::Close, settings);
	window.setFramerateLimit(60);
	window.setVerticalSyncEnabled(true);

	Clock gameTime;

	// Main game loop
	while(window.isOpen())
	{
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window : exit
			if (event.type == Event::Closed)
				window.close();
			else if(event.type == Event::LostFocus)
				level->DisableInput(true);
			else if(event.type == Event::GainedFocus)
				level->DisableInput(false);
			else if(event.type == Event::KeyPressed)
			{
				if(!started && Keyboard::isKeyPressed(Keyboard::Return))
				{
					started = true;
				}
				else if(started && Keyboard::isKeyPressed(Keyboard::Space))
				{
					paused = !paused;
				}
				else if(gameOver && Keyboard::isKeyPressed(Keyboard::Return))
				{
					gameOver = false;
					Restart();
				}
				else if(!gameOver && started && Keyboard::isKeyPressed(Keyboard::BackSpace))
					Restart();
			}
			// Set the window active even if user does not click directly on the window toolbar
			else if(level->IsInputDisabled() && event.type == Event::MouseButtonPressed)
			{
				Vector2i position = Mouse::getPosition(window);
				if(position.x > 0 && position.y > 0 && position.x < window.getSize().x && position.y < window.getSize().y)
					;//window.setFocus(true); // There is no such a method
			}
		}
		
		Time elapsed = gameTime.restart();
		if(!paused)
			Update(elapsed, window);
		Draw(window);
	}
}
void Game::Restart()
{
	level->Restart();
}
void Game::GameOver()
{
	gameOver = true;
	gameOverText = Text("You have survived: " + to_string(level->GetGameTime()) + " seconds\n" + "Press ENTER to restart the game", arial, 40);
	gameOverText.setColor(Color::Black);
	gameOverText.setPosition(windowSize.x * 0.25f, windowSize.y * 0.45f);

	gameOverRect = RectangleShape(Vector2f(gameOverText.getGlobalBounds().width + 32, gameOverText.getGlobalBounds().height + 32));
	gameOverRect.setPosition(gameOverText.getGlobalBounds().left - 16, gameOverText.getGlobalBounds().top - 16);
	gameOverRect.setFillColor(Color(255, 255, 255, 200));
}
void Game::Update(Time& elapsed, RenderWindow& window)
{
	if(started && !gameOver)
		level->Update(elapsed, window);
}

void Game::Draw(RenderWindow& window)
{
	window.clear(Color(200, 200, 200));
	
	level->Draw(window);
	if(!started)
	{
		window.draw(startRect);
		window.draw(startGame);
	}
	else if(gameOver)
	{
		window.draw(gameOverRect);
		window.draw(gameOverText);
	}
	else if(paused)
	{
		window.draw(pausedRect);
		window.draw(pausedText);
	}
	
	window.display();
}