#ifndef GAME_H
#define GAME_H
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>
#include <memory>
#include "Level.h"

class Level;
class Game
{
public:
	Game();
	void Run(); // the whole game runs here, when Run() returns, game ends
	void GameOver();
private:
	std::unique_ptr<Level> level;
	bool started;
	bool gameOver;
	bool paused;
	sf::Font arial;
	sf::Text startGame;
	sf::RectangleShape startRect;
	sf::Text pausedText;
	sf::RectangleShape pausedRect;
	sf::Text gameOverText;
	sf::RectangleShape gameOverRect;
	sf::Vector2i windowSize;

	void Initialize();
	void Update(sf::Time&, sf::RenderWindow&);
	void Draw(sf::RenderWindow&);
	inline void Restart();
};

#endif