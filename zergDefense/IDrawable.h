#ifndef IDRAWABLE_H
#define IDRAWABLE_H
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

class IDrawable
{
public:
	virtual void Draw(sf::RenderWindow& window)=0;
	sf::Vector2f GetPosition() {return position;}
protected:
	sf::Vector2f position;
	sf::Texture* texture;
	sf::Sprite sprite;
};

#endif