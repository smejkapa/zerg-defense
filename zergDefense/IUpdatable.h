#ifndef IUPDATABLE_H
#define IUPDATABLE_H
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>

class IUpdatable
{
public:
	virtual void Update(sf::Time& elapsed, sf::RenderWindow& window)=0;
};

#endif