#ifndef KEYBOARD_STATE_H
#define KEYBOARD_STATE_H
#include <SFML\Window.hpp>
#include <map>

/****************************
Extends functionallity of basic SFML Keyboard
Contais whole keyboard state as it was at one specific moment
*****************************/
class KeyboardState
{
public:
	KeyboardState()
	{
		for(int i = sf::Keyboard::Key::A; i < sf::Keyboard::Key::KeyCount; ++i)
			state.insert(std::make_pair((sf::Keyboard::Key)i, false));
	}
	bool IsKeyDown(sf::Keyboard::Key key) {return state[key];}
	inline void Refresh() 
	{
		for(int i = sf::Keyboard::Key::A; i < sf::Keyboard::Key::KeyCount; ++i)
			state[(sf::Keyboard::Key)i] = sf::Keyboard::isKeyPressed((sf::Keyboard::Key)i);
	}
private:
	std::map<sf::Keyboard::Key, bool> state;
};

#endif