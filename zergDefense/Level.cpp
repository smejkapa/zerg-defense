#include "Level.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include "CustomExceptions.h"

using namespace sf;
using namespace std;

Level::Level(Game* game) : levelNumber(0), mapPath("level0.map"), cfgPath("level0.cfg"), game(game)
{
	Initialize();
	LoadConfig();
	BasicTower::PreLoadResources(content);
	LongRangeTower::PreLoadResources(content);
	LoadContent();
}
Level::~Level()
{
	for(int y = 0; y < map.size(); ++y)
		for(int x = 0; x < map[y].size(); ++x)
			delete map[y][x];
}
void Level::Initialize()
{
	player = unique_ptr<Player>(new Player(this));
	map = Map();
	enemies = EnemyVector();
	directions = vector<Vector2i>();
	directions.push_back(Vector2i(-1, 0));
	directions.push_back(Vector2i(1, 0));
	directions.push_back(Vector2i(0, -1));
	directions.push_back(Vector2i(0, 1));
	mapWidth = mapHeight = 0;
	start = finish = Vector2i(-1,-1);
	LoadMap();
	GeneratePath(Vector2i(map[0][0]->getSize().x / 2, map[0][0]->getSize().y / 2), Vector2i(map[0][0]->getSize()));
	inputDisabled = false;
	
	stageStartCd = 10.f;
	toStageStart = stageStartCd;
	gameTime = -stageStartCd;

	escapeMax = 0;
	escaped = 0;
	lastRoach = false;
	stage = 1;
	timeFromLastSpawn = - stageStartCd * 1000;
	epsilonMax = 0;
	epsilon = 0;
	epsilonD = 0;
	lingProb = .5f;
	lingProbD = 0;
	roachProb = .5f;
	roachProbD = 0;
	lingTime = 3000;
	lingTimeD = 0;
	roachTime = 5000;
	roachTimeD = 0;
	stageTime = 60;
}
void Level::LoadContent()
{
	if((arial = content.GetFont(string("Fonts/arial.ttf"))) == nullptr)
		arial = content.LoadFont(string("Fonts/arial.ttf"));
}
void Level::LoadMap()
{
	ifstream reader;
	reader.open(mapPath);
	if(!reader.good())
		throw Exception("Can not find map file: " + mapPath);

	reader >> mapWidth;
	reader >> mapHeight;

	if(mapWidth <= 0 || mapHeight <= 0)
		throw Exception("Wrong map file format, width and height must be positive integers. " + mapPath);

	for(int y = 0; y < mapHeight; ++y)
	{
		map.push_back(vector<Tile*>());
		for (int x = 0; x < mapWidth; ++x)
		{
			char c;
			reader >> c;
			map[y].push_back(LoadTile(c, x, y));
		}
	}
	mapSize = Vector2i(mapWidth* map[0][0]->getSize().x, mapHeight * map[0][0]->getSize().y);
	if(start.x == -1 || finish.x == -1)
		throw Exception("There must be both start and finish on the map");
}
Tile* Level::LoadTile(char c, int x, int y)
{
	switch (c)
	{
	default:
	case '#':
		return new ImpassableTile(x, y, content);
	case '.':
		return new PassableTile(x, y, content);
	case 'S':
		if(start.x != -1)
			throw Exception("Only one start allowed");
		start = Vector2i(x,y);
		return new PassableTile(x, y, content);
	case 'F':
		if(finish.x != -1)
			throw Exception("Only one finish allowed");
		finish = Vector2i(x,y);
		return new PassableTile(x, y, content);
	case '_':
		return new BuildableTile(x, y, content);
	}
}
void Level::GeneratePath(Vector2i& offset, Vector2i& scale)
{
	Vector2i v = start;
	Vector2i tmp = v;
	while(v != finish)
	{
		Checkpoint c = Checkpoint(Vector2f(v.x * scale.x + offset.x, v.y * scale.y + offset.y), (Vector2f)v, false);
		path.push_back(c);

		Vector2i previous = tmp;
		tmp = v;
		for(int i = 0; i < directions.size(); ++i)
		{
			Vector2i next = Vector2i(tmp.x + directions[i].x, tmp.y + directions[i].y);
			if(next == previous) // Don't go backwards
				continue;
			if(next.x >= 0 && next.x < mapWidth && next.y >= 0 && next.y < mapHeight) // Is index in range?
				if(map[next.y][next.x]->IsPassable())
				{
					if(v != tmp) // We have already assigned new value -> there is more than one path
						throw Exception("There must be only one path in the map " + to_string(v.x) + ":" + to_string(v.y));
					v = next;
				}
		}
		if(v == tmp) // We have not moved anywhere
			throw Exception("Nowhere to go form: " + to_string(v.x) + ":" + to_string(v.y));
	}
	// One more for the finish
	Checkpoint c = Checkpoint(Vector2f(v.x * scale.x + offset.x, v.y * scale.y + offset.y), (Vector2f)v, true);
	path.push_back(c);
}
void Level::LoadConfig()
{
	ifstream reader;
	reader.open(cfgPath);
	// If there is no config the game will play on it's default properties
	if(!reader.good())
	{
		cout << "WARNING: No config loaded" << endl;
		return;
	}

	int lineNo = 0;
	string line;
	while(getline(reader, line))
	{
		++lineNo;
		istringstream iss(line);
		vector<string> split;
		do
		{
			string sub;
			iss >> sub;
			split.push_back(sub);
		} while (iss);
		
		if(split.size() <= 2)
			continue;
		
		try
		{
			if(split[0] == "money")
			{
				startMoney = stoi(split[1]);
				player->SetMoney(startMoney);
			}
			else if(split[0] == "stageTime")
			{
				stageTime = stoi(split[1]);
			}
			else if(split[0] == "escapeMax")
			{
				escapeMax = stoi(split[1]);
			}
			else if(split[0] == "stage")
			{
				if(split.size() < 6)
					throw Exception("ERROR: Config at  line: " + to_string(lineNo) + " not enough arguments");
				lingProb = stof(split[1]);
				roachProb = stof(split[2]);
				lingTime = stoi(split[3]);
				roachTime = stoi(split[4]);
				epsilonMax = stoi(split[5]);
			}
			else if(split[0] == "stageNext")
			{
				if(split.size() < 6)
					throw Exception("ERROR: Config at  line: " + to_string(lineNo) + " not enough arguments");
				lingProbD = stof(split[1]);
				roachProbD = stof(split[2]);
				lingTimeD = stoi(split[3]);
				roachTimeD = stoi(split[4]);
				epsilonD = stoi(split[5]);
			}
		}
		catch (invalid_argument e)
		{
			cout << "ERROR: Config at line: " << lineNo << " number expected" << endl;
			throw Exception("Fix the error and restart the game");
		}
	}

	cout << "Config loaded!" << endl;
}
void Level::Restart()
{
	Initialize();
	LoadConfig();
}

// Update
void Level::Update(sf::Time& elapsed, sf::RenderWindow& window)
{
	// The order of updates matters - check before reordering
	// Try to keep the blocks together (enemy block for instance)
	gameTime += elapsed.asSeconds();

	if(escaped > escapeMax)
		game->GameOver();

	// Check the game stage
	if(gameTime > stage * stageTime)
		SetNextStage();

	// Enemies
	int time = lastRoach ? roachTime : lingTime;
	timeFromLastSpawn += elapsed.asMilliseconds();
	if(timeFromLastSpawn > time + epsilon)
	{
		SpawnEnemy();
		timeFromLastSpawn = 0;
		epsilon = rand() % epsilonMax;
	}
	for(int i = 0; i < enemies.size(); ++i)
		enemies[i]->Update(elapsed, window);
	// Clean up
	ClenFinishedEnemies();

	// Player
	player->Update(elapsed, window);	
}
void Level::ClenFinishedEnemies()
{
	auto fin = remove_if(enemies.begin(), enemies.end(), [](shared_ptr<Enemy> e){return e->IsFinished();});
	escaped += enemies.end() - fin;
	enemies.erase(fin, enemies.end());
}
void Level::SpawnEnemy()
{
	if((float)rand() / RAND_MAX <= lingProb) // Gives number in (0,1)
	{
		lastRoach = false;
		enemies.push_back(shared_ptr<Enemy>(new Ling(path[0].GetPosition(), content, &path, stage - 1)));
	}
	else
	{
		lastRoach = true;
		enemies.push_back(shared_ptr<Enemy>(new Roach(path[0].GetPosition(), content, &path, stage - 1)));
	}
}
bool Level::Kill(shared_ptr<Enemy>& enemy)
{
	auto e = find(enemies.begin(), enemies.end(), enemy);
	if(e != enemies.end())
	{
		enemies.erase(e);
		return true;
	}
	return false;
}
void Level::SetNextStage()
{
	++stage;
	lingTime += lingTimeD;
	roachTime += roachTimeD;
	lingProb += lingProbD;
	roachProb += roachProbD;
	epsilonMax += epsilonD;
}

// Drawing
void Level::Draw(sf::RenderWindow& window)
{
	// Draw tiles first - bottom layer
	for(int y = 0; y < map.size(); ++y)
		for(int x = 0; x < map[y].size(); ++x)
			map[y][x]->Draw(window);

	for(int i = 0; i < enemies.size(); ++i)
		enemies[i]->Draw(window);

	player->Draw(window);

	// User Interface - Time
	string minutes = to_string((int)gameTime/60);
	bool minus = false;
	int s = ((int)gameTime % 60);
	string sec = to_string(s);
	if(s < 10 && s >= 0)
		sec = "0" + to_string(s);
	if(s < 0)
	{
		minus = true;
		sec = to_string(-s);
	}
	string str = minutes + ":" + sec;
	str = minus ? "-" + str : str;
	str = "Time: " + str + "     escaped: " + to_string(escaped) + "/" + to_string(escapeMax);
	Text text = Text(str, *arial, 20);
	text.setColor(Color::Black);
	text.setPosition(mapSize.x + 2, 2);
	window.draw(text);
}
void Level::DrawString(sf::RenderWindow& window, sf::Text& text, sf::Vector2i& offset, sf::Text::Style style, std::string& sToPrint)
{
	text.move(offset.x, offset.y);
	text.setStyle(style);
	text.setString(sToPrint);
	window.draw(text);
}
Tile* Level::ClickOn(Vector2i& position)
{
	// We are somewhere on the map -> the click is valid
	if(position.x > 0 && position.y > 0 &&
		position.x < mapSize.x && position.y < mapSize.y)
	{
		int x = position.x / map[0][0]->getSize().x;
		int y = position.y / map[0][0]->getSize().y;

		return map[y][x];
	}

	return nullptr;
}