#ifndef LEVEL_H
#define LEVEL_H
#include <vector>
#include <string>
#include "Game.h"
#include "IUpdatable.h"
#include "Player.h"
#include "Tile.h"
#include "Checkpoint.h"
#include "Enemy.h"
#include "ContentHolder.h"

class Game;
class Player;
class Tile;
class Enemy;
class Level : public IUpdatable, public IDrawable
{
public:
	Level(Game* game);
	virtual ~Level();
	virtual void Update(sf::Time& elapsed, sf::RenderWindow& window) override;
	virtual void Draw(sf::RenderWindow&) override;
	Player* GetPlayer() {return player.get();}
	bool IsInputDisabled() {return inputDisabled;}
	void DisableInput(bool value) {inputDisabled = value;}

	// Reinitialize to the same map but start the game over
	void Restart();
	int GetGameTime() {return (int)gameTime;}
	int GetStartMoney() {return startMoney;}

// Content
	ContentHolder content;

// Input
	Tile* ClickOn(sf::Vector2i& position);

// GUI
	sf::Vector2f GetGUIPosition() {return sf::Vector2f((float)mapSize.x, 32);}
	static void DrawString(sf::RenderWindow&, sf::Text&, sf::Vector2i&, sf::Text::Style, std::string&);

// Enemies
	typedef std::vector<Checkpoint> Path;
	typedef std::vector<std::shared_ptr<Enemy>> EnemyVector;
	
	// Kills an enemy - does not mean the enemy is destroyed 
	// but it doesn't exist for level anymore
	bool Kill(std::shared_ptr<Enemy>&);

	// Only living and active enemies are returned
	const EnemyVector& GetEnemies() {return enemies;}

	// All consequences of finished enemies are processed here
	// Game does not end here
	void ClenFinishedEnemies();
private:
	typedef std::vector<std::vector<Tile*>> Map;
	bool inputDisabled;
	std::unique_ptr<Player> player;
	sf::Font* arial;
	void LoadContent();
	
// Game logic
	Game* game;
	float gameTime;
	float stageStartCd;
	float toStageStart;
	int stage;
	int stageTime;
	int startMoney;
	void SetNextStage();
	
// Map
	Map map;
	std::string mapPath;
	std::string cfgPath;
	int levelNumber;
	sf::Vector2i mapSize; // in pixels
	int mapWidth; // in Tiles
	int mapHeight; // in Tiles

// Initializing
	void Initialize();
	void LoadMap();
	Tile* LoadTile(char c, int x, int y);
	void GeneratePath(sf::Vector2i&, sf::Vector2i&);
	void LoadConfig();

// Enemies
	EnemyVector enemies;
	sf::Vector2i start;
	sf::Vector2i finish;
	Path path;
	std::vector<sf::Vector2i> directions;
	void SpawnEnemy();
	bool lastRoach;
	float timeFromLastSpawn;
	float lingProb;
	float roachProb;
	int lingTime;
	int roachTime;
	int epsilonMax;
	int epsilon;
	int escapeMax;
	int escaped;
	// Delta values
	float lingProbD;
	float roachProbD;
	int lingTimeD;
	int roachTimeD;
	int epsilonD;
};
#endif