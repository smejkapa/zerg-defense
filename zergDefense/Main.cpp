#include "Game.h"
#include "CustomExceptions.h"
#include <iostream>

using namespace std;

int main()
{
	try
	{
		Game game;
		game.Run();
	}
	catch(Exception e)
	{
		cout << e.GetMessage() << endl;
		cout << "Press ENTER to exit...";
		string s;
		getline(cin, s);
	}
}