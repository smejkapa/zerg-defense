#include "Player.h"
#include "Level.h"
#include <iostream>
#include <algorithm>
#include "CustomExceptions.h"

using namespace std;
using namespace sf;

ContentHolder& Player::GetContent() {return level->content;}

Player::Player(Level* level) : level(level)
{
	Initialize();
	LoadContent(level->content);
}
void Player::Initialize()
{
	if(level != nullptr)
		money = level->GetStartMoney();
	else
		money = 200;
	lastGenerate = 0;
	forceGenerate = true;
	selectedTile = nullptr;
	wasMousePressed = false;
	currentKeyboard.Refresh();
	previousKeyboard.Refresh();
}
void Player::LoadContent(ContentHolder& content)
{
	if((arial = content.GetFont(string("Fonts/arial.ttf"))) == nullptr)
		arial = content.LoadFont(string("Fonts/arial.ttf"));

	SoundBuffer* sb;
	if((sb = content.GetSoundBuffer(string("Sounds/buttonclick.wav"))) == nullptr)
		sb = content.LoadSoundBuffer(string("Sounds/buttonclick.wav"));
	clickSnd.setBuffer(*sb);
	clickSnd.setVolume(15);

	if((sb = content.GetSoundBuffer(string("Sounds/action_deny.wav"))) == nullptr)
		sb = content.LoadSoundBuffer(string("Sounds/action_deny.wav"));
	actionDenySnd.setBuffer(*sb);
	actionDenySnd.setVolume(20);
}
// API
void Player::RemoveBuilding(shared_ptr<Building>& b)
{
	auto i = find(buildings.begin(), buildings.end(), b);
	if(i != buildings.end())
		buildings.erase(i);
}

// Update
void Player::Update(sf::Time& elapsed, sf::RenderWindow& window)
{
	for(size_t i = 0; i < buildings.size(); ++i)
		buildings[i]->Update(elapsed, window);
	for(size_t i = 0; i < projectiles.size(); ++i)
		projectiles[i]->Update(elapsed, window);
	auto rBegin = remove_if(projectiles.begin(), projectiles.end(), [](shared_ptr<Projectile> p) {return p->IsDone();});
	projectiles.erase(rBegin, projectiles.end());

	HandleInput(elapsed, window);

	if(forceGenerate || lastGenerate > 100)
	{
		GenerateActions(level->content);
		forceGenerate = false;
		lastGenerate = 0;
	}
	else
	{
		lastGenerate += elapsed.asMilliseconds();
	}
}
void Player::HandleInput(sf::Time& elapsed, const sf::RenderWindow& window)
{
	if(level->IsInputDisabled())
		return;

	if(Mouse::isButtonPressed(Mouse::Left))
	{
		// Respond to one click only once -> don't respond to pressed button more than once
		if(!wasMousePressed)
		{
			//cout << "Handling input " << endl;
			wasMousePressed = true;
			Vector2i position = Mouse::getPosition(window);
			
			// is the click even in the game window?
			if(position.x > 0 && position.x < window.getSize().x
				&& position.y > 0 && position.y < window.getSize().y)
			{
				//cout << "Click on: " << position.x << " " << position.y << endl;
				Click(window);
			}
		}
	}
	else
		wasMousePressed = false;
	
	// Keyboard input
	previousKeyboard = currentKeyboard;
	currentKeyboard.Refresh();

	int actionIdx = -1;
	if(IsNewKey(Keyboard::Key::Num1))
		actionIdx = 0;
	else if (IsNewKey(Keyboard::Key::Num2))
		actionIdx = 1;
	else if (IsNewKey(Keyboard::Key::Num3))
		actionIdx = 2;
	else if(IsNewKey(Keyboard::Key::Num4))
		actionIdx = 3;

	if(actionIdx >= 0 && (unsigned)actionIdx < actions.size())
	{
		if(!actions[actionIdx]->CheckAndExecute(selectedTile, *this))
			actionDenySnd.play();
	}
}
void Player::Click(const RenderWindow& window)
{
	forceGenerate = true;
	if(selectedTile != nullptr)
	{
		selectedTile->Deselect();
		selectedTile = nullptr;
	}
	selectedTile = level->ClickOn(Mouse::getPosition(window));
	if(selectedTile != nullptr)
	{
		selectedTile->Select();
		clickSnd.play();
	}
}
inline bool Player::IsNewKey(Keyboard::Key key)
{
	return currentKeyboard.IsKeyDown(key) && !previousKeyboard.IsKeyDown(key);
}
void Player::GenerateActions(ContentHolder& content)
{
	if(selectedTile != nullptr)
		actions = selectedTile->GenerateActions(level->content, level);
	else
		actions = Tile::ActionList();
}
void Player::ProcessKill(Projectile* projectile)
{
	float moneyMult = 1.0f;
	Tower* t = projectile->GetTower();
	auto tower = find_if(buildings.begin(), buildings.end(), [t](shared_ptr<Building> b){return t == b.get();});
	if(tower != buildings.end())
	{
		t->GainExp(projectile->GetTarget()->GetXpBounty());
		moneyMult = t->GetMoneyMultiplier();
	}
	money += floor(projectile->GetTarget()->GetMoneyBounty() * moneyMult);

	level->Kill(projectile->GetTarget());
}

// Draw
void Player::Draw(RenderWindow& window)
{
	for(size_t i = 0; i < buildings.size(); ++i)
		buildings[i]->Draw(window);
	for(size_t i = 0; i < projectiles.size(); ++i)
		projectiles[i]->Draw(window);

	DrawGUI(window);
}
void Player::DrawGUI(RenderWindow& window)
{
	Vector2f position = level->GetGUIPosition();
	// Background
	RectangleShape bg;
	bg.setPosition(Vector2f(position.x + 3, position.y +3));
	bg.setSize(Vector2f(512 - 6, 768)); 
	bg.setFillColor(Color(106, 153, 223, 255));
	bg.setOutlineColor(Color::Black);
	bg.setOutlineThickness(3);
	window.draw(bg);

	position = Vector2f(position.x + 10, position.y + 10);
	Text text = PresetActionTextFormat();
	text.setPosition(position.x, position.y);
	
	// Money
	text.setString("$" + to_string(money));
	text.setStyle(Text::Bold);
	window.draw(text);
	text.move(0, (float)text.getCharacterSize()+2);

	// Info about the tile
	text.setString(selectedTile == nullptr ? "No tile selected" : selectedTile->GetName());
	text.setStyle(Text::Underlined);
	window.draw(text);
	text.setStyle(Text::Regular);
	text.move(0, (float)text.getCharacterSize()+2);

	// Actions
	if(actions.size() == 0)
	{
		text.setString("No actions available");
		window.draw(text);
	}
	else
	{
		for(size_t i = 0; i < actions.size(); ++i)
		{
			text.setString(to_string(i+1) + ". " + actions[i]->GetName());
			Color textColor = actions[i]->IsAvailable(selectedTile, *this) ? Color::Black : Color(128, 128, 128);
			text.setColor(textColor);
			window.draw(text);
			text.move(0, (float)text.getCharacterSize() + 2);
		}
	}
	// Draw selected building details
	if(selectedTile != nullptr && selectedTile->GetBuilding() != nullptr)
	{
		selectedTile->GetBuilding()->DrawDetails(window, text, Vector2i(0, text.getCharacterSize()+2));
	}
}
Text Player::PresetActionTextFormat()
{
	Text text;
	text.setFont(*arial);
	text.setCharacterSize(20);
	text.setColor(Color::Black);

	return text;
}