#ifndef PLAYER_H
#define PLAYER_H
#include <SFML\Audio.hpp>
#include <vector>
#include <memory>
#include "IUpdatable.h"
#include "Level.h"
#include "Tile.h"
#include "Building.h"
#include "Action.h"
#include "KeyboardState.h"
#include "ContentHolder.h"

class Level;
class Action;
class Tile;
class Building;
class Projectile;
class Player : public IUpdatable, public IDrawable
{
public:
	Player(Level* level);
	ContentHolder& GetContent();

	void SetMoney(int m) {money = m;}
	bool CanBuy(int price) {return price <= money;}
	void Buy(int price) {money -= price;}
	void Earn(int price) {money += price;}

	virtual void Update(sf::Time& elapsed, sf::RenderWindow& window) override;
	virtual void Draw(sf::RenderWindow&) override;
	
	void AddBuilding(std::shared_ptr<Building> building) {buildings.push_back(building);}
	void RemoveBuilding(std::shared_ptr<Building>& building);
	
	void ProcessKill(Projectile*);
	void AddProjectile(std::shared_ptr<Projectile> p) {projectiles.push_back(p);}
private:
	int money;
	std::vector<std::shared_ptr<Building>> buildings;
	// Projectiles must be here in case the building is destroyed
	std::vector<std::shared_ptr<Projectile>> projectiles;
	Level* level;

	// Times in milliseconds
	bool forceGenerate;
	float lastGenerate;

	// Building
	Tile* selectedTile;

// Init
	void Initialize();
	void LoadContent(ContentHolder&);

// GUI
	sf::Font* arial;
	std::vector<std::shared_ptr<Action>> actions;
	void DrawGUI(sf::RenderWindow&);
	void GenerateActions(ContentHolder& content);
	sf::Text PresetActionTextFormat();

// Sounds
	sf::Sound clickSnd;
	sf::Sound actionDenySnd;

// Input
	bool wasMousePressed;
	KeyboardState currentKeyboard;
	KeyboardState previousKeyboard;
	bool IsNewKey(sf::Keyboard::Key);

	void HandleInput(sf::Time& elapsed, const sf::RenderWindow& window);
	void Click(const sf::RenderWindow&);
};
#endif