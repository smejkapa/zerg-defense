#include "Tile.h"
#include "CustomExceptions.h"

using namespace std;
using namespace sf;

// Tile
Color Tile::selectedColor = Color(100, 250, 50, 90);

Tile::Tile(int x, int y, ContentHolder& content, std::string& texturePath, string name, shared_ptr<Building> building) : building(building), selected(false), name(name)
{
	LoadContent(content, texturePath);
	sprite = Sprite(*texture);
	position = Vector2f(x * texture->getSize().x, y * texture->getSize().y);
	sprite.setPosition(position.x, position.y);
}
void Tile::LoadContent(ContentHolder& content, string& texturePath)
{
	if((texture = content.GetTexture(texturePath)) == nullptr)
		texture = content.LoadTexture(texturePath);
}
void Tile::Draw(RenderWindow& window)
{
	window.draw(sprite);
	if(selected)
	{
		RectangleShape rect(Vector2f(texture->getSize()));
		rect.setPosition(position);
		rect.setFillColor(Tile::selectedColor);
		window.draw(rect, RenderStates(BlendAlpha));
	}
}

// PassableTile
string PassableTile::TexturePath = "Textures/Tiles/passable.png";
PassableTile::PassableTile(int x, int y, ContentHolder& content, shared_ptr<Building> building) : Tile(x, y, content, PassableTile::TexturePath, "Passable tile", building)
{
}
// ImpassableTile
string ImpassableTile::TexturePath = "Textures/Tiles/notPassable.png";
ImpassableTile::ImpassableTile(int x, int y, ContentHolder& content, shared_ptr<Building> building) : Tile(x, y, content, ImpassableTile::TexturePath, "Impassable tile", building)
{
}

// BuildableTile
string BuildableTile::TexturePath = "Textures/Tiles/buildable.png";
BuildableTile::BuildableTile(int x, int y, ContentHolder& content, shared_ptr<Building> building) : Tile(x, y, content, BuildableTile::TexturePath, "Buildable tile", building)
{
}
BuildableTile::ActionList BuildableTile::GenerateActions(ContentHolder& content, Level* level)
{
	ActionList actions;
	actions.push_back(shared_ptr<Action>(new BuildAction(shared_ptr<Building>(new BasicTower(content, level, level->GetPlayer())))));
	actions.push_back(shared_ptr<Action>(new BuildAction(shared_ptr<Building>(new LongRangeTower(content, level, level->GetPlayer())))));
	actions.push_back(shared_ptr<Action>(new UpgradeAction(building)));
	actions.push_back(shared_ptr<Action>(new DestroyAction(building)));
	return actions;
}