#ifndef TILE_H
#define TILE_H
#include "IDrawable.h"
#include "Building.h"
#include "Action.h"
#include <memory>
#include "ContentHolder.h"

class Action;
class Tile : public IDrawable
{
public:
	typedef std::vector<std::shared_ptr<Action>> ActionList;

	Tile(int x, int y, ContentHolder& content, std::string& texturePath, std::string name, std::shared_ptr<Building> building = nullptr);
	
	sf::Vector2u getSize() {return texture->getSize();}
	void Select() {selected=true;}
	void Deselect() {selected=false;}
	std::shared_ptr<Building>& GetBuilding() {return building;}
	void SetBuilding(std::shared_ptr<Building>& b) {building = b;}
	void RemoveBuilding() {building = nullptr;}

	virtual void Draw(sf::RenderWindow&) override;
	virtual bool IsBuildable()=0;
	virtual bool IsPassable()=0;
	bool IsSelected() {return selected;}
	virtual std::string GetName() {return name;}
	virtual ActionList GenerateActions(ContentHolder& content, Level*)=0;
protected:	
	bool selected;
	std::shared_ptr<Building> building;
	static sf::Color selectedColor;
	std::string name;

	void LoadContent(ContentHolder& content, std::string& texturePath);
};

class PassableTile : public Tile
{
public:
	PassableTile(int x, int y, ContentHolder& content, std::shared_ptr<Building> building = nullptr);
	virtual bool IsBuildable() override {return false;}
	virtual bool IsPassable() override {return true;}
	virtual ActionList GenerateActions(ContentHolder& content, Level*) override {return ActionList();};
private:
	static std::string TexturePath;
	static std::string TextureName;
};

class ImpassableTile : public Tile
{
public:
	ImpassableTile(int x, int y, ContentHolder& content, std::shared_ptr<Building> building = nullptr);
	virtual bool IsBuildable() override {return false;}
	virtual bool IsPassable() override {return false;}
	virtual ActionList GenerateActions(ContentHolder& content, Level*) override {return ActionList();}
private:
	static std::string TexturePath;
	static std::string TextureName;
};

class BuildableTile : public Tile
{
public:
	BuildableTile(int x, int y, ContentHolder& content, std::shared_ptr<Building> building = nullptr);
	virtual bool IsBuildable() override {return building == nullptr;}
	virtual bool IsPassable() override {return false;}
	virtual ActionList GenerateActions(ContentHolder& content, Level*) override;
private:
	static std::string TexturePath;
	static std::string TextureName;
};
#endif